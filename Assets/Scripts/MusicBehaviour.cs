﻿using UnityEngine;
using System.Collections;

public class MusicBehaviour : MonoBehaviour {
	public AudioSource _AudioSource1;
	public AudioSource _AudioSource2;
	public AudioSource _AudioSource3;

	public GameObject wolfs;

	private string state = "";
	// Use this for initialization
	void Start () {
		_AudioSource1.Play();
	}
	
	// Update is called once per frame
	void Update () {
		int a = wolfs.transform.childCount;

		for (int i = 0; i < a; i++)
		{
			Transform wolf=wolfs.transform.GetChild (i);
			WolfAI script = wolf.gameObject.GetComponent<WolfAI>();
			if(script.state=="hunting" && ( state=="" || state=="hunting")){state=script.state;}
			else if(script.state=="alerte" && state!="chase"){state=script.state;}
			else if(script.state=="chase"){state=script.state;}
				//_AudioSource1.Stop();

			}

			//_AudioSource3.Play();

		if (state == "hunting") {
						if (!_AudioSource1.isPlaying)
								_AudioSource1.Play ();
						if (_AudioSource3.isPlaying)
								_AudioSource3.Stop ();
						if (_AudioSource2.isPlaying)
								_AudioSource2.Stop ();
				} else if (state == "alerte") {
						if (_AudioSource1.isPlaying)
								_AudioSource1.Stop ();
						if (_AudioSource2.isPlaying)
								_AudioSource2.Stop ();
						if (!_AudioSource3.isPlaying)
								_AudioSource3.Play ();
				} else if (state == "chase") {
						if (_AudioSource1.isPlaying)
								_AudioSource1.Stop ();
						if (_AudioSource3.isPlaying)
								_AudioSource3.Stop ();
						if (!_AudioSource2.isPlaying)
								_AudioSource2.Play ();
				}
		state = "";

	}
}
