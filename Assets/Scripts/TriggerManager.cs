﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TriggerManager : MonoBehaviour {
	public int count;
	public int innitial_count;
	public int actual_total_sheeps;

	public GameObject sheeps;

	private List<Collider> colided;
	private bool end;
	Rect Rectangle = new Rect(10, 10, 200, 150);

	// Use this for initialization
	void Start () {
		end = false;
		colided = new List<Collider>();
		count = 0;
		innitial_count = sheeps.transform.childCount;
	}
	
	// Update is called once per frame
	void Update () {
		int act = 0;
		//on compte le nombre de moutons survivants
		actual_total_sheeps= sheeps.transform.childCount;
		for (int i = 0; i < actual_total_sheeps; i++) {
				Transform sheep = sheeps.transform.GetChild (i);
			if(sheep.gameObject.activeSelf)act+=1;
				}
		actual_total_sheeps = act;

		//2 élément par mouton
		count=colided.Count/2;

		if (count == actual_total_sheeps || actual_total_sheeps==0) {
			end = true; 
			Time.timeScale=0;
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Sheep" && !colided.Contains(other)) {
						//count += 1;
						colided.Add(other);
				}
	}
	void OnTriggerExit(Collider other) {
		if (other.gameObject.tag == "Sheep" && colided.Contains (other)) {
						//count -= 1;
						colided.Remove (other);
				}
	}
	void OnGUI() {

		//fin!
		if (end == true) {
						Rect rect = new Rect (0, 0, 100, 40);
						GUI.Window (0, Rectangle, DoMyWindow, "End Menu");
				}		
		}

	//menu de fin
	void DoMyWindow(int windowID) {
		GUI.Box (new Rect (10, 20, 150, 20), "END score:" + ((float)actual_total_sheeps / (float)innitial_count)*100 + "%");
		if (GUI.Button (new Rect (10, 60, 100, 20), "Restart")) {
						Time.timeScale=1;
			end=false;
			count=0;
						Application.LoadLevel (Application.loadedLevelName);
				}
		if (GUI.Button (new Rect (10, 90, 100, 20), "Quit")) {
			Application.Quit();
		}
	}
}
