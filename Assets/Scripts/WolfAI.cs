﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WolfAI : MonoBehaviour {


	public float patrolSpeed = 2f;							// The nav mesh agent's speed when patrolling.
	public float chaseSpeed = 5f;							// The nav mesh agent's speed when chasing.
	public float eatTimer = 5f;
	public float dogFearTimer = 5f;
	public float eatableDistance = 1.0f;
	public string state;

	public float dogFearSpeed = 5.0f;
	public float normalSpeed= 3.0f;
	public float huntSpeed = 6.0f;

	private NavMeshAgent nav;								// Reference to the nav mesh agent.
	private Transform player;								// Reference to the player's transform.
	private float actualDogFearTimer = 0;
	private float actualEatTimer = 0;
	private GameObject target;
	private GameObject obstacle_for_wolf;
	private List<GameObject> list;

	// Use this for initialization
	void Start () {
		nav = GetComponent<NavMeshAgent>();
		player = GameObject.FindGameObjectWithTag("Player").transform;
		obstacle_for_wolf = GameObject.FindGameObjectWithTag("obstacle_parent");
		state = "hunting";
		list = new List<GameObject>();
		target = null;
	}
	
	// Update is called once per frame
	void Update () {

		if (state == "alerte") 
		{
			if (Vector3.Distance (transform.position, nav.destination) <= nav.stoppingDistance) {
						actualDogFearTimer += Time.deltaTime;
						if (actualDogFearTimer >= dogFearTimer) {
								state = "hunting";
								nav.speed = huntSpeed;
								actualDogFearTimer = 0.0f;
						}
			}
		} 
		else if (state == "chase") 
		{
			if (Vector3.Distance (transform.position, target.transform.position) <= eatableDistance) 
			{			
				actualEatTimer += Time.deltaTime;
				if (actualEatTimer >= eatTimer) 
				{
						target.SetActive (false);
						target=null;
						state = "hunting";
						nav.speed = normalSpeed;
						actualEatTimer = 0.0f;
				}
			}
		} 
		else if (state == "hunting") 
		{
			if (Vector3.Distance (transform.position, nav.destination) <= nav.stoppingDistance) 
			{


				//GameObject bloqo = Resources.Load("obstacle", typeof(GameObject));
				//GameObject ayo = Resources.LoadAssetAtPath<GameObject>("Prefabs/obstacle.prefab");
				GameObject ayo= (GameObject)Resources.LoadAssetAtPath("Assets/Prefabs/obstacle.prefab",typeof(GameObject));
				//GameObject bloq = Instantiate(ayo,transform.position,transform.rotation)as GameObject;
				//GameObject bloqo = GameObject.FindGameObjectWithTag("obstacle");
				//GameObject.
				/*GameObject bloq = Instantiate(bloqo);*/

				ayo.transform.parent=obstacle_for_wolf.transform;
				GameObject bloq = Instantiate(ayo,transform.position,transform.rotation)as GameObject;
				//bloq.transform.parent=obstacle_for_wolf.transform;
				bloq.transform.parent=obstacle_for_wolf.transform;;
				NavMeshObstacle obs = bloq.GetComponent<NavMeshObstacle>();
				obs.radius = 10.0f;

				//GameObject[] list = ga;



				/*int children = obstacle_for_wolf.transform.childCount;
				Debug.Log(children);*/
				list.Add(bloq);
				Debug.Log(bloq);
				/*for (int i = 0; i < children; ++i)
				{
					list.Add(transform.GetChild(i).gameObject);
					//Debug.Log(transform.GetChild(i));

				}*/
				//list.add(transform.GetChild(i));
				foreach (GameObject o in list) 
				{
					o.SetActive(true);

					Debug.Log(o.activeSelf);
				}
				//GameObject[] list = GameObject.FindGameObjectsWithTag("obstacle");
				//foreach (var elmt in list) {elmt.SetActive(true);}

				float walkRadius = 15.0f;
				Vector3 randomDirection = Random.insideUnitSphere * walkRadius;
				randomDirection += transform.position;
				NavMeshHit hit;
				NavMesh.SamplePosition(randomDirection, out hit, walkRadius, 1);
				Vector3 finalPosition = hit.position;
				nav.destination = finalPosition;

				foreach (GameObject o in list) 
				{
					o.SetActive(false);
				}
				//foreach (var elmt in list) {elmt.SetActive(false);}
							
							

			}
		}
	}

	void OnTriggerStay (Collider other)
	{
				// If the player has entered the trigger sphere...
				if (other.gameObject.tag == "Player") {
					nav.destination = transform.position + (transform.position - player.position);
					state= "alerte";
					nav.speed = dogFearSpeed;
				}
		else if (other.gameObject.tag == "Sheep" && (state == "hunting" || state=="chase")){//
					if (target==null)
						target = other.gameObject;
					nav.destination = target.transform.position;
					//Debug.Log(target.transform.position);
					state= "chase";
					nav.speed = chaseSpeed;
					
				}
	}

	void OnGUI() {
		Camera camera_temp =GameObject.FindGameObjectWithTag("MainCamera").camera;
		Vector3 screenPos = camera_temp.WorldToScreenPoint(transform.position);
		if (screenPos.z > 0) 
		{
			Rect rect = new Rect (screenPos.x, camera_temp.pixelHeight - screenPos.y, 100, 40);
			GUI.Box (rect, state);
		}
	}
}
