﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SheepAI : MonoBehaviour {

	public string state ;
	public int count = 0;
	public float dogFearTimer =2.0f;
	public float wolfFearTimer =4.0f;
	public float followFearTimer= 3.0f;

	public float dogFearSpeed = 5.0f;
	public float wolfFearSpeed = 7.0f;
	public float normalSpeed = 3.0f;
	public float followSpeed = 5.0f;
	

	private float randomTimer = 0;
	private float actualDogFearTimer =0.0f;
	private float actualWolfFearTimer =0.0f;
	private float actualFollowFearTimer =0.0f;
	private float actualRandomTimer = 0.0f;
	private NavMeshAgent nav;								// Reference to the nav mesh agent.
	private List<GameObject> SheepList = new List<GameObject>();
	private Vector3 neib_bary = new Vector3(0,0,0);
	private float update_counter=0.0f;

	// Use this for initialization
	void Start () {
		nav = GetComponent<NavMeshAgent>();
		state = "eat_grass";
		randomTimer=Random.Range(1.0f,7.0f);
	}
	
	// Update is called once per frame
	void Update () {
		count =SheepList.Count;

		if (state == "eat_grass") 
		{
			nav.speed =normalSpeed;
			actualRandomTimer+=Time.deltaTime;
			print (nav.destination);
			if (actualRandomTimer >= randomTimer)
			{
				actualRandomTimer=0.0f;
				randomTimer=Random.Range(2.0f,7.0f);

				float walkRadius = 2.0f;
				Vector3 randomDirection = Random.insideUnitSphere * walkRadius;
				randomDirection += transform.position;
				NavMeshHit hit;
				NavMesh.SamplePosition(randomDirection, out hit, walkRadius, 1);
				Vector3 finalPosition = hit.position;
				nav.destination = finalPosition;
			}
		}
		else if (state == "?")
		{
			nav.speed =followSpeed;
			actualFollowFearTimer+=Time.deltaTime;
			if (actualFollowFearTimer >= followFearTimer)
			{
				actualFollowFearTimer=0.0f;
				state = "eat_grass";
			}
		}
		else if (state == "dog_alerte")
			{   
			nav.speed =dogFearSpeed;
			actualDogFearTimer+=Time.deltaTime;
			if (actualDogFearTimer >= dogFearTimer)
			{
				Debug.Log("there");
				actualDogFearTimer=0.0f;
				state = "eat_grass";
			}
		}
		else if (state == "wolf_alerte")
		{
			nav.speed =wolfFearSpeed;
			actualWolfFearTimer+=Time.deltaTime;
			if (actualWolfFearTimer >= wolfFearTimer)
			{
				actualWolfFearTimer=0.0f;
				state = "eat_grass";
			}
		}

		//calcul du barycentre (futur, boid)
		update_counter += Time.deltaTime;
		if (update_counter>1 )
		{
			update_counter=0.0f;
			neib_bary = new Vector3 (0, 0, 0);
			foreach( GameObject o in SheepList )
			{
				neib_bary+=o.transform.position;
			}
			neib_bary = neib_bary / SheepList.Count;
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Sheep" && !SheepList.Contains(other.gameObject))
		{
			SheepList.Add(other.gameObject);
		}
	}
	void OnTriggerExit (Collider other)
	{
		if (other.gameObject.tag == "Sheep") 
		{
			SheepList.Remove(other.gameObject);
		}
	}
	void OnTriggerStay (Collider other)
	{

		if (other.gameObject.tag == "Sheep" && state == "eat_grass" )
		{
			float dist=Vector3.Distance(transform.position,other.transform.position);
			SheepAI script = other.gameObject.GetComponent<SheepAI>();
			if ((script.state == "dog_alerte" || script.state == "wolf_alerte") && dist<4)
			{
				state = "?";
				NavMeshAgent nav_other = other.gameObject.GetComponent<NavMeshAgent>();

				Vector3 Dir = nav_other.destination - other.transform.position;
				nav.destination= transform.position + Dir ;
			}
		}
		// If the player has entered the trigger sphere...
		if (other.gameObject.tag == "Player") 
		{
			Vector3 vect =(transform.position -other.transform.position);
			float dist=Vector3.Distance(transform.position,other.transform.position);
			//if (dist >1)
			nav.destination = transform.position + vect;

			state= "dog_alerte";
		}
		else if (other.gameObject.tag == "Wolf" && state != "dog_alerte")
		{
			nav.destination = transform.position+ (transform.position -other.transform.position);

			state= "wolf_alerte";
		}

	}

	void OnGUI() {
		Camera camera_temp =GameObject.FindGameObjectWithTag("MainCamera").camera;
		Vector3 screenPos = camera_temp.WorldToScreenPoint(transform.position);
		if (screenPos.z > 0) 
		{
			Rect rect = new Rect (screenPos.x, camera_temp.pixelHeight - screenPos.y, 100, 40);
			GUI.Box (rect, state);
		}
	}
}
